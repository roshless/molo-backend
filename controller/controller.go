package controller

import (
	"log/slog"
	"time"

	"git.roshless.me/molo/backend/api"
	"git.roshless.me/molo/backend/background"
	"git.roshless.me/molo/backend/config"
	"git.roshless.me/molo/backend/middleware/jwt"
	"git.roshless.me/molo/backend/middleware/metrics"
	"git.roshless.me/molo/backend/model"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/httplog/v2"
)

type Controller struct {
	Router         *chi.Mux
	Config         config.Config
	Models         model.Models
	Handlers       api.Handlers
	BackgroundSync background.BackgroundSync
	JWT            jwt.JWTObject
}

func NewController(cfg *config.Config, pool model.PgxIface) *Controller {
	c := &Controller{}
	c.Router = chi.NewRouter()
	c.Config = *cfg
	c.JWT = jwt.NewJWT(cfg.JWTSecret)
	c.Models = model.NewModels(pool)
	c.Handlers = api.NewHandlers(cfg, &c.Models, &c.JWT)
	c.BackgroundSync.Init(&c.Models, cfg)
	return c
}

func (c *Controller) MountHandlers() {
	logger := httplog.NewLogger("logger", httplog.Options{
		JSON:             true,
		LogLevel:         slog.LevelInfo,
		Concise:          false,
		RequestHeaders:   true,
		MessageFieldName: "message",
		TimeFieldFormat:  time.RFC3339,
	})

	// Register heartbeat before logging so it doesn't spam logs
	c.Router.Use(middleware.Heartbeat("/up"))

	c.Router.Use(middleware.RealIP)
	// RequestLogger automatically makes use of the chi RequestID and Recoverer middleware.
	c.Router.Use(httplog.RequestLogger(logger, []string{
		"/up",
		"/metrics",
	}))

	c.Router.Use(metrics.NewMiddleware())

	c.Router.Use(middleware.AllowContentType("application/json", "application/xml", "text/xml", "multipart/form-data", "text/plain"))

	c.Router.Mount("/api/v1/user", c.Handlers.Users.Routes())
	c.Router.Mount("/api/v1/feed", c.Handlers.Feeds.Routes())
	c.Router.Mount("/metrics", metrics.Handler())
}
