package api

import (
	"errors"
	"io"
	"net/http"
	"sort"
	"strconv"

	"git.roshless.me/molo/backend/middleware/jwt"
	"git.roshless.me/molo/backend/model"
	"git.roshless.me/molo/backend/opml"
	"git.roshless.me/molo/backend/payloads"
	"git.roshless.me/molo/backend/rss"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

type FeedHandler struct {
	feedModel    model.FeedModel
	entriesModel model.FeedEntryModel
	j            jwt.JWTObject
}

func (h FeedHandler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Group(func(r chi.Router) {
		r.Use(h.j.Verifier())
		r.Use(jwt.AuthenticatorIsLoggedIn)

		r.With(h.paginate).Get("/", h.listAll)
		r.Post("/", h.create)

		r.Post("/import", h.importFeeds)
		r.Get("/export", h.exportFeeds)

		r.Get("/unread_entries", h.getAllEntries)

		r.Route("/{feed_id}", func(r chi.Router) {
			r.Get("/", h.get)
			r.Patch("/", h.updateEntries)
			r.Put("/", h.update)
			r.Delete("/", h.delete)

			r.Route("/entries", func(r chi.Router) {
				r.Get("/", h.getEntries)
				r.Route("/{entry_id}", func(r chi.Router) {
					r.Get("/", h.getEntry)
					r.Patch("/", h.setReadEntry)
				})
			})
		})
	})

	r.Group(func(r chi.Router) {
		r.Use(h.j.Verifier())
		r.Use(jwt.AuthenticatorIsAdmin)

		r.With(h.paginate).Get("/all", h.listAll)
	})

	return r
}

func (h FeedHandler) listAll(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	feeds, err := h.feedModel.GetAll(r.Context(), strconv.Itoa(userID))
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	if err := render.RenderList(w, r, payloads.NewFeedListResponse(feeds)); err != nil {
		_ = render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (h FeedHandler) create(w http.ResponseWriter, r *http.Request) {
	// TODO catch duplicates before processing rss
	f := payloads.FeedRequest{}
	if err := render.Bind(r, &f); err != nil {
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest, Error: err.Error()},
		)
		return
	}
	f.UserID = jwt.GetUserID(r.Context())

	// verify sometime if this is a good idea
	feed, err := rss.ParseFeed(r.Context(), f.RssURL, f.Category, f.UserID)
	f.Feed = &feed
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	err = h.feedModel.Add(r.Context(), *f.Feed)
	if err != nil {
		if errors.Is(err, model.ErrConflict) {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusConflict,
				Error: http.StatusText(http.StatusConflict)},
			)
		} else {
			render.Render(w, r, payloads.InternalServerError(err))
		}
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func (h FeedHandler) get(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	feed, err := h.feedModel.Get(r.Context(), strconv.Itoa(userID), chi.URLParam(r, "feed_id"))
	if err == model.ErrNotFound {
		render.Render(w, r, payloads.NotFound(err))
		return
	} else if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}

	if err = render.Render(w, r, payloads.NewFeedResponse(feed)); err != nil {
		_ = render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (h FeedHandler) updateEntries(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	userID := jwt.GetUserID(r.Context())

	feed, err := h.feedModel.Get(ctx, strconv.Itoa(userID), chi.URLParam(r, "feed_id"))
	if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}
	entries, err := rss.ParseEntries(r.Context(), feed)
	if err != nil {
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest,
			Error: err.Error()},
		)
		return
	}
	err = h.entriesModel.Add(ctx, entries)
	if err != nil {
		if !errors.Is(err, model.ErrNoNewEntries) {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusBadRequest,
				Error: err.Error()},
			)
		}
	}
}

func (h FeedHandler) update(w http.ResponseWriter, r *http.Request) {
	f := payloads.FeedRequest{Type: payloads.FeedRequestUpdate}
	if err := render.Bind(r, &f); err != nil {
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest, Error: err.Error()},
		)
		return
	}
	f.UserID = jwt.GetUserID(r.Context())

	id, err := strconv.Atoi(chi.URLParam(r, "feed_id"))
	f.Id = id
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	err = h.feedModel.Update(r.Context(), *f.Feed)
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
	}
}

func (h FeedHandler) delete(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	rowsAffected, err := h.feedModel.Delete(r.Context(), strconv.Itoa(userID), chi.URLParam(r, "feed_id"))
	if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}
	if rowsAffected < 1 {
		render.Render(w, r, payloads.NotFound(err))
		return
	}
}

func (h FeedHandler) getEntries(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	var err error

	var entries []model.FeedEntry
	if r.URL.Query().Get("unread_only") == "true" {
		entries, err = h.entriesModel.GetAllUnreadOfFeed(r.Context(), strconv.Itoa(userID), chi.URLParam(r, "feed_id"))
	} else {
		entries, err = h.entriesModel.GetAllOfFeed(r.Context(), strconv.Itoa(userID), chi.URLParam(r, "feed_id"))
	}
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	if err := render.RenderList(w, r, payloads.NewFeedEntryListResponse(entries)); err != nil {
		_ = render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (h FeedHandler) getEntry(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	entry, err := h.entriesModel.Get(r.Context(), strconv.Itoa(userID), chi.URLParam(r, "feed_id"), chi.URLParam(r, "entry_id"))
	if err != nil {
		if errors.Is(err, model.ErrNotFound) {
			render.Render(w, r, payloads.NotFound(err))
		} else {
			render.Render(w, r, payloads.BadRequest(err))
		}
		return
	}

	if err := render.Render(w, r, payloads.NewFeedEntryResponse(entry)); err != nil {
		_ = render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (h FeedHandler) getAllEntries(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	var err error

	var entries []model.FeedEntry
	entries, err = h.entriesModel.GetAllUnread(r.Context(), strconv.Itoa(userID))
	if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}

	if err := render.RenderList(w, r, payloads.NewFeedEntryListResponse(entries)); err != nil {
		_ = render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (h FeedHandler) setReadEntry(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	err := h.entriesModel.SetRead(r.Context(), strconv.Itoa(userID), chi.URLParam(r, "feed_id"), chi.URLParam(r, "entry_id"))
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (h FeedHandler) importFeeds(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	formFile, _, err := r.FormFile("file")
	if err != nil {
		if formFile == nil {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusInternalServerError, Error: "missing *file* form part",
			})
			return
		}
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	bytes, err := io.ReadAll(formFile)
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	if len(bytes) == 0 {
		err = errors.New("empty body")
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest, Error: err.Error()},
		)
		return
	}

	feeds, err := opml.ImportFeeds(bytes)
	if err != nil {
		// Unexpected error
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	for _, f := range feeds {
		f.UserID = userID
		if err = f.IsCorrectlyInitialized(); err != nil {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusBadRequest,
				Error: err.Error()},
			)
			return
		}
		f, err = rss.ParseFeed(r.Context(), f.RssURL, f.Category, f.UserID)
		if err != nil {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusInternalServerError,
				Error: err.Error()},
			)
			return
		}
		err = h.feedModel.Add(r.Context(), f)
		if errors.Is(err, model.ErrConflict) {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusConflict,
				Error: http.StatusText(http.StatusConflict)},
			)
			return
		} else if err != nil {
			render.Render(w, r, payloads.InternalServerError(err))
			return
		}
	}
}

func (h FeedHandler) exportFeeds(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	feeds, err := h.feedModel.GetAll(r.Context(), strconv.Itoa(userID))
	if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}
	// Sort by tag
	// TODO maybe inside model?
	sort.Slice(feeds, func(i, j int) bool {
		return feeds[i].Category < feeds[j].Category
	})
	exportedFeeds, err := opml.FeedsToOPML(feeds)
	if err != nil {
		// Unexpected error
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}
	w.Write([]byte(exportedFeeds))
}

// TODO
// paginate is a stub, but very possible to implement middleware logic
// to handle the request params for handling a paginated request.
func (h FeedHandler) paginate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// just a stub.. some ideas are to look at URL query params for something like
		// the page number, or the limit, and send a query cursor down the chain
		next.ServeHTTP(w, r)
	})
}
