package api

import (
	"errors"
	"net/http"
	"strconv"
	"time"

	"git.roshless.me/molo/backend/middleware/jwt"
	"git.roshless.me/molo/backend/model"
	"git.roshless.me/molo/backend/payloads"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

type UserHandler struct {
	registrationEnabled bool
	model               model.UserModel
	j                   jwt.JWTObject
}

func (h UserHandler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Post("/", h.create)
	r.Post("/login", h.login)

	r.Group(func(r chi.Router) {
		r.Use(h.j.Verifier())
		r.Use(jwt.AuthenticatorIsLoggedIn)

		r.Get("/validate", func(w http.ResponseWriter, r *http.Request) {
			// Do nothing since this is already taken care by middelware
		})
		r.Patch("/", h.updateSelf)
		r.Get("/self", h.getSelf)
	})

	r.Group(func(r chi.Router) {
		r.Use(h.j.Verifier())
		r.Use(jwt.AuthenticatorIsAdmin)

		r.With(h.paginate).Get("/", h.list)
		r.Get("/search", h.search)
		r.Route("/{userID}", func(r chi.Router) {
			r.Get("/", h.get)
			r.Put("/", h.update)
			r.Delete("/", h.delete)
		})
	})

	return r
}

func (h UserHandler) list(w http.ResponseWriter, r *http.Request) {
	users, err := h.model.GetAll(r.Context())
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	if err := render.RenderList(w, r, payloads.NewUserListResponse(users)); err != nil {
		_ = render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (h UserHandler) create(w http.ResponseWriter, r *http.Request) {
	if !h.registrationEnabled {
		render.Render(w, r, payloads.Forbidden(errors.New("registration disabled")))
		return
	}

	u := payloads.UserRequest{}
	if err := render.Bind(r, &u); err != nil {
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest, Error: err.Error()},
		)
		return
	}

	if err := h.model.Add(r.Context(), *u.User); err != nil {
		switch {
		case errors.Is(err, model.ErrConflict):
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusConflict, Error: err.Error()},
			)
		default:
			// Unexpected error
			render.Render(w, r, payloads.InternalServerError(err))
		}
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func (h UserHandler) get(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(chi.URLParam(r, "userID"))
	if err != nil {
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest,
			Error: "userID param missing or not valid"},
		)
		return
	}

	user, err := h.model.Get(r.Context(), userID)
	if err == model.ErrNotFound {
		render.Render(w, r, payloads.NotFound(err))
		return
	} else if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}

	if err := render.Render(w, r, payloads.NewUserResponse(user)); err != nil {
		_ = render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (h UserHandler) getSelf(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	user, err := h.model.Get(r.Context(), userID)
	if err != nil {
		// Should be impossible
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	if err := render.Render(w, r, payloads.NewUserResponse(user)); err != nil {
		_ = render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (h UserHandler) update(w http.ResponseWriter, r *http.Request) {
	u := payloads.UserRequest{}
	if err := render.Bind(r, &u); err != nil {
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest, Error: err.Error()},
		)
		return
	}

	userID, err := strconv.Atoi(chi.URLParam(r, "userID"))
	if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}
	err = h.model.Update(r.Context(), userID, *u.User)

	if err != nil {
		switch {
		case errors.Is(err, model.ErrConflict):
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusConflict,
				Error: http.StatusText(http.StatusConflict)},
			)
		default:
			// Unexpected error
			render.Render(w, r, payloads.InternalServerError(err))
		}
	}
}

func (h UserHandler) updateSelf(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	// Get existingUser
	existingUser, err := h.model.Get(r.Context(), userID)
	if err != nil {
		// Should be impossible
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	newUser := payloads.UserRequest{User: &existingUser, Type: payloads.UserRequestNone}
	if err = render.Bind(r, &newUser); err != nil {
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest, Error: err.Error()},
		)
		return
	}

	err = h.model.UpdateSelf(r.Context(), userID, *newUser.User)
	if err != nil {
		switch {
		case errors.Is(err, model.ErrConflict):
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusConflict,
				Error: http.StatusText(http.StatusConflict)},
			)
		default:
			// Unexpected error
			render.Render(w, r, payloads.InternalServerError(err))
		}
	}
}

func (h UserHandler) search(w http.ResponseWriter, r *http.Request) {
	username := r.URL.Query().Get("username")

	user, err := h.model.Search(r.Context(), username)
	if err == model.ErrNotFound {
		render.Render(w, r, payloads.NotFound(err))
		return
	} else if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}

	if err := render.Render(w, r, payloads.NewUserResponse(user)); err != nil {
		_ = render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

// delete removes user, if used by admin.
func (h UserHandler) delete(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(chi.URLParam(r, "userID"))
	if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}

	rowsAffected, err := h.model.Delete(r.Context(), userID)
	if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}
	if rowsAffected < 1 {
		render.Render(w, r, payloads.NotFound(err))
		return
	}
}

// login returns in body Bearer JWT token valid for 3 days.
func (h UserHandler) login(w http.ResponseWriter, r *http.Request) {
	u := payloads.UserRequest{Type: payloads.UserRequestLogin}
	if err := render.Bind(r, &u); err != nil {
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest, Error: err.Error()},
		)
		return
	}

	if user, err := h.model.Authenticate(r.Context(), *u.User); err == nil {
		claims := map[string]interface{}{
			"sub": strconv.Itoa(user.Id), "name": user.Name, "isAdmin": user.IsAdmin,
		}
		// For some unknown reason we cant set any standard claim manually
		jwt.SetJWTTimeClaims(&claims, time.Hour*72)
		_, tokenString, err2 := h.j.Encode(claims)
		if err2 != nil {
			render.Render(w, r, payloads.InternalServerError(err))
			return
		}
		w.Write([]byte(tokenString))
	} else {
		render.Render(w, r, payloads.Unauthorized(err))
	}
}

// TODO
// paginate is a stub, but very possible to implement middleware logic
// to handle the request params for handling a paginated request.
func (h UserHandler) paginate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// just a stub.. some ideas are to look at URL query params for something like
		// the page number, or the limit, and send a query cursor down the chain
		next.ServeHTTP(w, r)
	})
}
