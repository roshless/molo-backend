package api

import (
	"git.roshless.me/molo/backend/config"
	"git.roshless.me/molo/backend/middleware/jwt"
	"git.roshless.me/molo/backend/model"
)

type Handlers struct {
	Users UserHandler
	Feeds FeedHandler
}

func NewHandlers(cfg *config.Config, m *model.Models, jwto *jwt.JWTObject) Handlers {
	return Handlers{
		Users: UserHandler{
			registrationEnabled: cfg.Registration_enabled,
			model:               m.Users,
			j:                   *jwto,
		},
		Feeds: FeedHandler{
			feedModel:    m.Feeds,
			entriesModel: m.FeedEntries,
			j:            *jwto,
		},
	}
}
