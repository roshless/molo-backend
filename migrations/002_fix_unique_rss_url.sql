ALTER TABLE molo_feed DROP CONSTRAINT molo_feed_rss_url_key;

---- create above / drop below ----

ALTER TABLE molo_feed ADD CONSTRAINT molo_feed_rss_url_key UNIQUE(rss_url);
