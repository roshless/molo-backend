CREATE EXTENSION pgcrypto;
CREATE EXTENSION citext;

CREATE TABLE molo_user (
	id 			BIGSERIAL PRIMARY KEY,
	name 			CITEXT NOT NULL UNIQUE,
	email 			CITEXT NOT NULL UNIQUE,
	password 		TEXT NOT NULL,
	is_admin 		BOOLEAN DEFAULT FALSE NOT NULL,
	update_interval 	INTERVAL NOT NULL
);

CREATE TABLE molo_category (
	id 			BIGSERIAL PRIMARY KEY,
	name 			TEXT NOT NULL UNIQUE
);

CREATE TABLE molo_feed (
	id 			BIGSERIAL PRIMARY KEY,
	user_id 		BIGSERIAL REFERENCES molo_user(id),
	category_id 		BIGSERIAL REFERENCES molo_category(id),
	name 			TEXT NOT NULL,
	author 			TEXT NOT NULL,
	url 			TEXT NOT NULL,
	rss_url 		TEXT NOT NULL UNIQUE,
	description 		TEXT,
	create_date 		TIMESTAMP WITH TIME ZONE,
	update_date 		TIMESTAMP WITH TIME ZONE
);

CREATE TABLE molo_entry (
	id 			BIGSERIAL PRIMARY KEY,
	feed_id 		BIGSERIAL REFERENCES molo_feed(id),
	user_id 		BIGSERIAL REFERENCES molo_user(id),
	name 			TEXT NOT NULL,
	url 			CITEXT NOT NULL,
	date 	 		TIMESTAMP WITH TIME ZONE,
	content 		TEXT,
	read 	 		BOOLEAN DEFAULT FALSE NOT NULL
);
