package rss

import (
	"context"
	"time"

	"git.roshless.me/molo/backend/model"
	"github.com/mmcdole/gofeed"
)

func ParseFeed(ctx context.Context, url string, category string, userID int) (model.Feed, error) {
	fp := gofeed.NewParser()
	f, err := fp.ParseURLWithContext(url, ctx)
	if err != nil {
		autodiscoveredUrl, err := rssAutodiscoverUrl(url)
		if err != nil {
			return model.Feed{}, err
		}

		f, err = fp.ParseURLWithContext(autodiscoveredUrl, ctx)
		if err != nil {
			return model.Feed{}, err
		}
	}
	// For some reason, gofeed cant parse <author>
	// TODO investigate
	var author string
	if len(f.Authors) > 0 {
		author = f.Authors[0].Name
	}
	return model.Feed{
		UserID:     userID,
		Category:   category,
		Name:       f.Title,
		Author:     author,
		URL:        f.Link,
		RssURL:     url,
		CreateDate: time.Now(),
		UpdateDate: time.Time{},
	}, nil
}

func ParseEntries(ctx context.Context, f model.Feed) ([]model.FeedEntry, error) {
	var addToDatabase []model.FeedEntry

	fp := gofeed.NewParser()
	parsedFeed, err := fp.ParseURLWithContext(f.RssURL, ctx)
	if err != nil {
		return []model.FeedEntry{}, err
	}
	for _, el := range parsedFeed.Items {
		if f.UpdateDate.Before(*el.PublishedParsed) {
			addToDatabase = append(addToDatabase, model.FeedEntry{
				FeedID:  f.Id,
				UserID:  f.UserID,
				Name:    el.Title,
				URL:     el.Link,
				Content: el.Content,
				Date:    *el.PublishedParsed,
			})
		}
	}
	return addToDatabase, nil
}
