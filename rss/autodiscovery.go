package rss

import (
	"context"
	"errors"
	"net/http"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func rssAutodiscoverUrl(site string) (string, error) {
	var rssLink string
	client := http.Client{}

	req, err := http.NewRequestWithContext(context.Background(), http.MethodGet, site, nil)
	if err != nil {
		return "", err
	}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		nodes, err := html.ParseFragment(resp.Body, &html.Node{
			Type:     html.ElementNode,
			Data:     "head",
			DataAtom: atom.Head,
		})
		if err != nil {
			return "", err
		}

		var rsslinkNode *html.Node
		for _, node := range nodes {
			if node.Data == "link" && node.DataAtom == atom.Link {
				for _, attr := range node.Attr {
					if attr.Val == "application/rss+xml" {
						rsslinkNode = node
					}
				}
			}
		}
		if rsslinkNode == nil {
			return "", errors.New("website " + site + " does not appear to have rss autodiscovery")
		}
		for _, el := range rsslinkNode.Attr {
			if el.Key == "href" {
				rssLink = el.Val
				return rssLink, err
			}
		}
	}
	return "", errors.New("rss autodiscovery- unexpected error")
}
