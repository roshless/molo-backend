package main

import (
	"context"
	"time"

	"git.roshless.me/molo/backend/config"
	"github.com/jackc/pgx/v5/pgxpool"
)

func connectDatabase(cfg *config.Config) (*pgxpool.Pool, error) {
	var pool *pgxpool.Pool
	poolConfig, err := pgxpool.ParseConfig(cfg.Database_connection_string)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	pool, err = pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil {
		return nil, err
	}

	return pool, nil
}
