package main

import (
	"flag"
	"net/http"

	"git.roshless.me/molo/backend/config"
	"git.roshless.me/molo/backend/controller"
)

func main() {
	configPath := flag.String("config", "", "Config location, leave empty to load from environment variables")
	flag.Parse()

	cfg := config.NewConfig(*configPath)

	db, err := connectDatabase(cfg)
	if err != nil {
		panic(err)
	}

	c := controller.NewController(cfg, db)

	c.MountHandlers()
	http.ListenAndServe(":"+c.Config.Port, c.Router)
}
