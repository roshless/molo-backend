package background

import (
	"context"
	"log/slog"
	"math/rand"
	"strconv"
	"time"

	"git.roshless.me/molo/backend/config"
	"git.roshless.me/molo/backend/middleware/metrics"
	"git.roshless.me/molo/backend/model"
	"git.roshless.me/molo/backend/rss"
	"github.com/prometheus/client_golang/prometheus"
)

type BackgroundSync struct {
	Models *model.Models
}

func (b BackgroundSync) Init(m *model.Models, cfg *config.Config) {
	if !cfg.Periodic_feeds_updates {
		slog.Info("Periodic feed updates disabled")
		return
	}

	b.Models = m

	// Context only for first db call
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	// TODO refresh users every X minutes
	users, err := b.Models.Users.GetAll(ctx)
	if err != nil {
		slog.Error("Periodic feed updates disabled due to error ", err)
		return
	}

	for _, u := range users {
		if u.UpdateInterval != 0 {
			go b.runUpdateLoop(u)
		}
	}
}

func (b BackgroundSync) runUpdateLoop(u model.User) {
	// Create new context inside because we are not waiting for goroutines inside IniT
	for {
		// If nothing comes out in 2 minutes there is no way feed link is working
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Minute)

		r := rand.Intn(10)
		time.Sleep(time.Duration(r) * time.Second)

		feeds, err := b.Models.Feeds.GetAll(ctx, strconv.Itoa(u.Id))
		if err != nil {
			slog.Error("backgroundsync- error getting feeds for user ", u.Name, err)
		}

		for _, f := range feeds {
			status := "OK"
			entries, err := rss.ParseEntries(ctx, f)
			if err != nil {
				status = err.Error()
				slog.Error("backgroundsync- error on feed ", f.Name, err)
			}
			err = b.Models.FeedEntries.Add(ctx, entries)
			if err != nil && err != model.ErrNoNewEntries {
				status = err.Error()
				slog.Error("Unexpected error", err)
			}
			metrics.UpdatedFeedsCounter.With(prometheus.Labels{
				"userID": strconv.Itoa(f.UserID), "status": status,
			}).Inc()
		}

		// Not defered because this function never ends
		cancel()
		time.Sleep(u.UpdateInterval)
	}
}
