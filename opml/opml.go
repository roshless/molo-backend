package opml

import (
	"time"

	"git.roshless.me/molo/backend/model"
	"github.com/gilliek/go-opml/opml"
)

func ImportFeeds(b []byte) ([]model.Feed, error) {
	// TODO make compatible with other rss readers?
	var out []model.Feed
	o, err := opml.NewOPML(b)
	if err != nil {
		return nil, err
	}
	for _, category := range o.Outlines() {
		for _, feed := range category.Outlines {
			// We only care about RSS URL and category name
			out = append(out, model.Feed{
				RssURL:   feed.XMLURL,
				Category: category.Text,
			})
		}
	}
	return out, nil
}

func FeedsToOPML(feeds []model.Feed) (string, error) {
	if len(feeds) == 0 {
		return "", nil
	}
	// TODO verify if this is alright with spec
	// http://opml.org/spec2.opml
	var out opml.OPML

	out.Head.Title = "molo v1"
	out.Head.DateCreated = time.Now().Format(time.RFC822)
	out.Head.OwnerName = feeds[0].User
	out.Version = "2.0"

	var lastCategory string
	var toAppend []opml.Outline
	for i, f := range feeds {
		if lastCategory != f.Category && i != 0 {
			out.Body.Outlines = append(out.Body.Outlines, opml.Outline{
				Text:     lastCategory,
				Outlines: toAppend,
			})
			toAppend = nil
		}
		lastCategory = f.Category
		toAppend = append(toAppend, opml.Outline{
			Created: f.CreateDate.Format(time.RFC822),
			XMLURL:  f.RssURL,
			URL:     f.URL,
			Title:   f.Name,
			Text:    f.Name,
			Type:    "rss",
		})
	}
	// TODO maybe rethink this loop, for each not so good here
	out.Body.Outlines = append(out.Body.Outlines, opml.Outline{
		Text:     lastCategory,
		Outlines: toAppend,
	})

	return out.XML()
}
