package payloads

import (
	"log/slog"
	"net/http"

	"github.com/go-chi/httplog/v2"
	"github.com/go-chi/render"
)

type ErrorResponse struct {
	Err            error  `json:"-"`
	HTTPStatusCode int    `json:"-"`
	Error          string `json:"error"`
}

func (e *ErrorResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	httplog.LogEntrySetField(r.Context(), "error", slog.StringValue(e.Err.Error()))
	return nil
}

func NotFound(err error) *ErrorResponse {
	return &ErrorResponse{
		Err:            err,
		HTTPStatusCode: http.StatusNotFound,
		Error:          http.StatusText(http.StatusNotFound),
	}
}

func BadRequest(err error) *ErrorResponse {
	return &ErrorResponse{
		Err:            err,
		HTTPStatusCode: http.StatusBadRequest,
		Error:          http.StatusText(http.StatusBadRequest),
	}
}

func Unauthorized(err error) *ErrorResponse {
	return &ErrorResponse{
		Err:            err,
		HTTPStatusCode: http.StatusUnauthorized,
		Error:          http.StatusText(http.StatusUnauthorized),
	}
}

func InternalServerError(err error) *ErrorResponse {
	return &ErrorResponse{
		Err:            err,
		HTTPStatusCode: http.StatusInternalServerError,
		Error:          http.StatusText(http.StatusInternalServerError),
	}
}

func Forbidden(err error) *ErrorResponse {
	return &ErrorResponse{
		Err:            err,
		HTTPStatusCode: http.StatusForbidden,
		Error:          http.StatusText(http.StatusForbidden),
	}
}
