package payloads

import (
	"errors"
	"net/http"

	"git.roshless.me/molo/backend/model"
	"github.com/go-chi/render"
)

const (
	FeedRequestFull   = 0
	FeedRequestNone   = 1
	FeedRequestUpdate = 2
)

type FeedResponse struct {
	*model.Feed
}

func NewFeedResponse(feed model.Feed) *FeedResponse {
	return &FeedResponse{Feed: &feed}
}

func NewFeedListResponse(feeds []model.Feed) []render.Renderer {
	list := []render.Renderer{}
	for _, feed := range feeds {
		list = append(list, NewFeedResponse(feed))
	}
	return list
}

func (f *FeedResponse) Bind(r *http.Request) error {
	return nil
}

func (f *FeedResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

type FeedRequest struct {
	*model.Feed
	Type int
}

func (f *FeedRequest) Bind(r *http.Request) error {
	if f.Feed == nil {
		return errors.New("missing feed")
	}

	switch f.Type {
	case FeedRequestFull:
		if err := f.IsCorrectlyInitialized(); err != nil {
			return err
		}
	case FeedRequestUpdate:
		if err := f.IsUpdatedCorrectly(); err != nil {
			return err
		}
	case FeedRequestNone:
	}
	return nil
}
