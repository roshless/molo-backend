package payloads

import (
	"net/http"

	"git.roshless.me/molo/backend/model"
	"github.com/go-chi/render"
)

type FeedEntryResponse struct {
	*model.FeedEntry
}

func NewFeedEntryResponse(feedEntry model.FeedEntry) *FeedEntryResponse {
	return &FeedEntryResponse{FeedEntry: &feedEntry}
}

func NewFeedEntryListResponse(entries []model.FeedEntry) []render.Renderer {
	list := []render.Renderer{}
	for _, entry := range entries {
		list = append(list, NewFeedEntryResponse(entry))
	}
	return list
}

func (f *FeedEntryResponse) Bind(r *http.Request) error {
	return nil
}

func (f *FeedEntryResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
