package payloads

import (
	"errors"
	"net/http"

	"git.roshless.me/molo/backend/model"
	"github.com/go-chi/render"
)

const (
	UserRequestFull  = 0
	UserRequestLogin = 1
	UserRequestNone  = 2
)

type UserResponse struct {
	*model.User
}

func NewUserResponse(user model.User) *UserResponse {
	return &UserResponse{User: &user}
}

func NewUserListResponse(users []model.User) []render.Renderer {
	list := []render.Renderer{}
	for _, user := range users {
		list = append(list, NewUserResponse(user))
	}
	return list
}

func (u *UserResponse) Bind(r *http.Request) error {
	return nil
}

func (u *UserResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

type UserRequest struct {
	*model.User
	Type int
}

func (u *UserRequest) Bind(r *http.Request) error {
	if u.User == nil {
		return errors.New("missing user")
	}

	switch u.Type {
	case UserRequestFull:
		if err := u.IsCorrectlyInitialized(); err != nil {
			return err
		}
	case UserRequestLogin:
		if err := u.HasLoginCredentials(); err != nil {
			return err
		}
	case UserRequestNone:
		// No field required to exist
	}
	return nil
}
