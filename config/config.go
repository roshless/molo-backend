package config

import (
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Port                       string `yaml:"port"`
	Periodic_feeds_updates     bool   `yaml:"periodic_feeds_updates"`
	Registration_enabled       bool   `yaml:"registration_enabled"`
	Database_connection_string string `yaml:"database_connection_string"`
	JWTSecret                  string `yaml:"jwt_secret"`
}

func configFromEnvs(cfg *Config) {
	cfg.Port = os.Getenv("MOLO_PORT")
	cfg.Database_connection_string = os.Getenv("MOLO_CONNECTION_STRING")
	if os.Getenv("MOLO_REGISTRATION_ENABLED") != "" {
		cfg.Registration_enabled = true
	}
	cfg.JWTSecret = os.Getenv("MOLO_SECRET")
}

func NewConfig(path string) *Config {
	cfg := &Config{}

	if path == "" {
		log.Println("Loading config from environment variables")
		configFromEnvs(cfg)
		return cfg
	}

	file, err := os.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal(file, &cfg)
	if err != nil {
		log.Fatal(err)
	}
	return cfg
}
