package jwt

import (
	"context"
	"errors"
	"log"
	"net/http"
	"strconv"
	"time"

	"git.roshless.me/molo/backend/payloads"
	"github.com/go-chi/jwtauth/v5"
	"github.com/go-chi/render"
	"github.com/lestrrat-go/jwx/v2/jwt"
)

type JWTObject struct {
	jwtAuth *jwtauth.JWTAuth
}

// Verifier is just here to limit external imports in api
func (j JWTObject) Verifier() func(http.Handler) http.Handler {
	return jwtauth.Verifier(j.jwtAuth)
}

func (j JWTObject) Encode(claims map[string]interface{}) (jwt.Token, string, error) {
	return j.jwtAuth.Encode(claims)
}

func NewJWT(secret string) JWTObject {
	j := JWTObject{}
	j.jwtAuth = jwtauth.New("HS512", []byte(secret), nil)
	return j
}

// SetJWTTimeClaims is a workaround for setting exp and iat (that should be done at creation time).
func SetJWTTimeClaims(claims *map[string]interface{}, expireIn time.Duration) {
	jwtauth.SetExpiryIn(*claims, expireIn)
	jwtauth.SetIssuedNow(*claims)
}

func AuthenticatorIsLoggedIn(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, _, err := jwtauth.FromContext(r.Context())
		if err != nil {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusUnauthorized,
				Error: err.Error()},
			)
			return
		}

		if token == nil || jwt.Validate(token) != nil {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusUnauthorized,
				Error: http.StatusText(http.StatusUnauthorized)},
			)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// AuthenticatorIsAdmin is a middleware for checking user's admin privilege.
func AuthenticatorIsAdmin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, claims, err := jwtauth.FromContext(r.Context())

		if err != nil {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusUnauthorized,
				Error: err.Error()},
			)
			return
		}

		if token == nil || jwt.Validate(token) != nil {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusUnauthorized,
				Error: http.StatusText(http.StatusUnauthorized)},
			)
			return
		}

		// Not admin
		if !claims["isAdmin"].(bool) {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusUnauthorized,
				Error: http.StatusText(http.StatusUnauthorized)},
			)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func GetUserID(context context.Context) int {
	// We don't check for errors since this is always being run after verifing existing token
	token, _, _ := jwtauth.FromContext(context)
	idInt, err := strconv.Atoi(token.Subject())
	if err != nil {
		// Just in case for now
		// TODO remove after running in the wild/testing more
		log.Fatalln("error while converting user id from JWT sub claim")
	}
	return idInt
}

// TODO verify if this ever returns any error, it shouldnt
func GetUsername(context context.Context) (string, error) {
	_, claims, err := jwtauth.FromContext(context)
	if err != nil {
		return "", err
	}
	username, ok := claims["name"].(string)
	if ok {
		return username, nil
	}
	return "", errors.New("could not get username from token")
}
