package model

import (
	"context"
	"time"

	"github.com/jackc/pgx/v5"
)

type FeedEntry struct {
	Id         int       `json:"id"`
	FeedID     int       `json:"feed_id"`
	Feed       string    `json:"feed"`
	UserID     int       `json:"user_id"`
	User       string    `json:"user"`
	Name       string    `json:"name"`
	URL        string    `json:"url"`
	Content    string    `json:"content"`
	Date       time.Time `json:"date"`
	CategoryID int       `json:"category_id"`
	Category   string    `json:"category"`
	Read       bool      `json:"read"`
}

type FeedEntryModel struct {
	DB PgxIface
	FeedEntryModelInterface
}

func (m FeedEntryModel) Add(ctx context.Context, f []FeedEntry) error {
	if len(f) == 0 {
		return ErrNoNewEntries
	}

	var rows [][]interface{}

	for _, el := range f {
		rows = append(rows, []interface{}{el.FeedID, el.UserID, el.Name, el.URL, el.Date, el.Content})
	}

	sqlUpdateFeed := `UPDATE molo_feed 
	    SET update_date = NOW() 
	    WHERE id = $1;`

	tx, err := m.DB.Begin(context.Background())
	if err != nil {
		return errorConvert(err)
	}
	defer tx.Rollback(ctx)

	_, err = tx.CopyFrom(
		ctx,
		pgx.Identifier{"molo_entry"},
		[]string{"feed_id", "user_id", "name", "url", "date", "content"},
		pgx.CopyFromRows(rows),
	)
	if err != nil {
		return errorConvert(err)
	}

	_, err = tx.Exec(ctx, sqlUpdateFeed, f[0].FeedID)
	if err != nil {
		return errorConvert(err)
	}

	tx.Commit(ctx)
	return errorConvert(err)
}

// TODO pagination?
func (m FeedEntryModel) GetAllOfFeed(ctx context.Context, userID, feedID string) ([]FeedEntry, error) {
	sql := `SELECT e.id, u.name as username, e.user_id, 
		f.name as feed_name, e.feed_id, e.name, 
		e.url, e.date, c.name AS category, f.category_id AS category_id,
		e.content, e.read 
		FROM molo_entry AS e 
		JOIN molo_user AS u ON e.user_id = u.id 
		JOIN molo_feed AS f ON e.feed_id = f.id
		JOIN molo_category AS c ON f.category_id = c.id
		WHERE e.user_id = $1 AND e.feed_id = $2
		ORDER BY e.date`

	var entries []FeedEntry

	rows, err := m.DB.Query(ctx, sql, userID, feedID)
	if err != nil {
		return nil, errorConvert(err)
	}
	defer rows.Close()

	for rows.Next() {
		var e FeedEntry
		err = rows.Scan(&e.Id, &e.User, &e.UserID, &e.Feed, &e.FeedID, &e.Name,
			&e.URL, &e.Date, &e.Category, &e.CategoryID, &e.Content, &e.Read)
		if err != nil {
			return nil, errorConvert(err)
		}
		entries = append(entries, e)
	}
	if err = rows.Err(); err != nil {
		return nil, errorConvert(err)
	}
	return entries, errorConvert(err)
}

// TODO pagination?
func (m FeedEntryModel) GetAllUnreadOfFeed(ctx context.Context, userID, feedID string) ([]FeedEntry, error) {
	sql := `SELECT e.id, u.name as username, e.user_id, 
		f.name as feed_name, e.feed_id, e.name, 
		e.url, e.date, c.name AS category, f.category_id AS category_id,
		e.content, e.read 
		FROM molo_entry AS e 
		JOIN molo_user AS u ON e.user_id = u.id 
		JOIN molo_feed AS f ON e.feed_id = f.id
		JOIN molo_category AS c ON f.category_id = c.id
		WHERE e.user_id = $1 AND e.feed_id = $2 AND e.read = false
		ORDER BY e.date`

	var entries []FeedEntry

	rows, err := m.DB.Query(ctx, sql, userID, feedID)
	if err != nil {
		return nil, errorConvert(err)
	}
	defer rows.Close()

	for rows.Next() {
		var e FeedEntry
		err = rows.Scan(&e.Id, &e.User, &e.UserID, &e.Feed, &e.FeedID, &e.Name,
			&e.URL, &e.Date, &e.Category, &e.CategoryID, &e.Content, &e.Read)
		if err != nil {
			return nil, errorConvert(err)
		}
		entries = append(entries, e)
	}
	if err = rows.Err(); err != nil {
		return nil, errorConvert(err)
	}
	return entries, errorConvert(err)
}

// TODO pagination?
func (m FeedEntryModel) GetAllUnread(ctx context.Context, userID string) ([]FeedEntry, error) {
	sql := `SELECT e.id, u.name as username, e.user_id, 
		f.name as feed_name, e.feed_id, e.name, 
		e.url, e.date, c.name AS category, f.category_id AS category_id,
		e.content, e.read 
		FROM molo_entry AS e 
		JOIN molo_user AS u ON e.user_id = u.id 
		JOIN molo_feed AS f ON e.feed_id = f.id
		JOIN molo_category AS c ON f.category_id = c.id
		WHERE e.user_id = $1 AND e.read = false
		ORDER BY e.date`

	var entries []FeedEntry

	rows, err := m.DB.Query(ctx, sql, userID)
	if err != nil {
		return nil, errorConvert(err)
	}
	defer rows.Close()

	for rows.Next() {
		var e FeedEntry
		err = rows.Scan(&e.Id, &e.User, &e.UserID, &e.Feed, &e.FeedID, &e.Name,
			&e.URL, &e.Date, &e.Category, &e.CategoryID, &e.Content, &e.Read)
		if err != nil {
			return nil, errorConvert(err)
		}
		entries = append(entries, e)
	}
	if err = rows.Err(); err != nil {
		return nil, errorConvert(err)
	}
	return entries, errorConvert(err)
}

func (m FeedEntryModel) Get(ctx context.Context, userID, feedID, entryID string) (FeedEntry, error) {
	sql := `SELECT e.id, u.name as username, e.user_id, 
		f.name as feed_name, e.feed_id, e.name, 
		e.url, e.date, c.name AS category, f.category_id AS category_id,
		e.content, e.read 
		FROM molo_entry AS e 
		JOIN molo_user AS u ON e.user_id = u.id 
		JOIN molo_feed AS f ON e.feed_id = f.id
		JOIN molo_category AS c ON f.category_id = c.id
		WHERE e.user_id = $1 AND e.feed_id = $2 AND e.id = $3`

	var e FeedEntry

	rows := m.DB.QueryRow(ctx, sql, userID, feedID, entryID)
	err := rows.Scan(&e.Id, &e.User, &e.UserID, &e.Feed, &e.FeedID, &e.Name,
		&e.URL, &e.Date, &e.Category, &e.CategoryID, &e.Content, &e.Read)
	return e, errorConvert(err)
}

func (m FeedEntryModel) SetRead(ctx context.Context, userID, feedID, entryID string) error {
	sql := `UPDATE molo_entry
		SET read = true
		WHERE id = $1 AND feed_id = $2 AND user_id = $3`

	_, err := m.DB.Exec(ctx, sql, entryID, feedID, userID)
	return errorConvert(err)
}

// Delete should only be used before removing the corresponding feed.
func (m FeedEntryModel) Delete(ctx context.Context, userID, feedID string) (int, error) {
	sql := `DELETE FROM molo_entry
		WHERE user_id = $1 AND feed_id = $2`

	tag, err := m.DB.Exec(ctx, sql, userID, feedID)
	return int(tag.RowsAffected()), errorConvert(err)
}
