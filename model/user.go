package model

import (
	"context"
	"errors"
	"time"
)

type User struct {
	Id             int           `json:"id"`
	Name           string        `json:"name"`
	Email          string        `json:"email"`
	Password       string        `json:"password"`
	IsAdmin        bool          `json:"is_admin"`
	UpdateInterval time.Duration `json:"update_interval"`
}

func (u User) IsCorrectlyInitialized() error {
	// Don't care about ID and admin field.
	if u.Name == "" {
		return errors.New("missing user username")
	}
	if u.Email == "" {
		return errors.New("missing user email")
	}
	if u.Password == "" {
		return errors.New("missing user password")
	}

	if u.UpdateInterval.Minutes() < 5 || u.UpdateInterval.Minutes() > 240 {
		return errors.New("user update interval should be between 5 minues and 4 hours")
	}

	return nil
}

func (u User) HasLoginCredentials() error {
	if u.Name == "" {
		return errors.New("missing username")
	} else if u.Password == "" {
		return errors.New("missing password")
	}
	return nil
}

type UserModel struct {
	DB PgxIface
	UserModelInterface
}

func (m UserModel) Add(ctx context.Context, user User) error {
	sql := `INSERT INTO molo_user (
		   name, password, email, is_admin, update_interval 
	       ) values ($1, crypt($2, gen_salt('bf')), $3, $4, $5)`

	_, err := m.DB.Exec(ctx, sql, user.Name, user.Password, user.Email, user.IsAdmin, user.UpdateInterval)
	return errorConvert(err)
}

func (m UserModel) GetAll(ctx context.Context) ([]User, error) {
	sql := `SELECT id, name, email, is_admin, update_interval FROM molo_user
		ORDER BY name`

	rows, err := m.DB.Query(ctx, sql)
	if err != nil {
		return nil, errorConvert(err)
	}
	defer rows.Close()

	var users []User
	for rows.Next() {
		var u User
		err = rows.Scan(&u.Id, &u.Name, &u.Email, &u.IsAdmin, &u.UpdateInterval)
		if err != nil {
			return nil, errorConvert(err)
		}
		users = append(users, u)
	}
	if err = rows.Err(); err != nil {
		return nil, errorConvert(err)
	}
	return users, errorConvert(err)
}

// Search returns user based on username.
func (m UserModel) Search(ctx context.Context, username string) (User, error) {
	sql := `SELECT id, name, email, is_admin, update_interval FROM molo_user
		WHERE name = $1`

	row := m.DB.QueryRow(ctx, sql, username)

	var user User
	err := row.Scan(&user.Id, &user.Name, &user.Email, &user.IsAdmin, &user.UpdateInterval)
	return user, errorConvert(err)
}

// Get returns user based on user ID.
func (m UserModel) Get(ctx context.Context, userID int) (User, error) {
	sql := `SELECT id, name, email, is_admin, update_interval FROM molo_user
		WHERE id = $1`

	row := m.DB.QueryRow(ctx, sql, userID)

	var user User
	err := row.Scan(&user.Id, &user.Name, &user.Email, &user.IsAdmin, &user.UpdateInterval)
	return user, errorConvert(err)
}

func (m UserModel) Delete(ctx context.Context, userID int) (int, error) {
	sql := `DELETE FROM molo_user
		WHERE id = $1`

	tag, err := m.DB.Exec(ctx, sql, userID)
	return int(tag.RowsAffected()), errorConvert(err)
}

// TODO partial update ?
func (m UserModel) Update(ctx context.Context, userID int, u User) error {
	sql := `UPDATE molo_user SET name = $1, password = crypt($2, gen_salt('bf')), email = $3, is_admin = $4, update_interval = $5
		WHERE id = $6`

	_, err := m.DB.Exec(ctx, sql, u.Name, u.Password, u.Email, u.IsAdmin, u.UpdateInterval, userID)
	return errorConvert(err)
}

// UpdateSelf is the update query but you can't change your username
func (m UserModel) UpdateSelf(ctx context.Context, userID int, u User) error {
	var err error
	// If new password is empty it just wasn't supplied.
	if u.Password == "" {
		sql := `UPDATE molo_user SET email = $1, update_interval = $2
		WHERE id = $3`
		_, err = m.DB.Exec(ctx, sql, u.Email, u.UpdateInterval, userID)
	} else {
		sql := `UPDATE molo_user SET password = crypt($1, gen_salt('bf')), email = $2, update_interval = $3
		WHERE id = $4`
		_, err = m.DB.Exec(ctx, sql, u.Password, u.Email, u.UpdateInterval, userID)
	}
	return errorConvert(err)
}

// Authenticate returns true if credentials correct.
func (m UserModel) Authenticate(ctx context.Context, u User) (User, error) {
	sql := `SELECT id, name, email, is_admin, update_interval FROM molo_user 
		WHERE name = $1 AND password = crypt($2, password)`

	row := m.DB.QueryRow(ctx, sql, u.Name, u.Password)
	var user User
	err := row.Scan(&user.Id, &user.Name, &user.Email, &user.IsAdmin, &user.UpdateInterval)
	return user, errorConvert(err)
}
