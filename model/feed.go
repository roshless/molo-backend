package model

import (
	"context"
	"errors"
	"time"
)

type Feed struct {
	Id         int       `json:"id"`
	UserID     int       `json:"user_id"`
	User       string    `json:"user"`
	CategoryID int       `json:"category_id"`
	Category   string    `json:"category"`
	Name       string    `json:"name"`
	Author     string    `json:"author"`
	URL        string    `json:"url"`
	RssURL     string    `json:"rss_url"`
	CreateDate time.Time `json:"create_date"`
	UpdateDate time.Time `json:"update_date"`
}

func (f Feed) IsCorrectlyInitialized() error {
	if f.Category == "" {
		return errors.New("missing feed category")
	}
	if f.RssURL == "" {
		return errors.New("missing feed rss link")
	}

	return nil
}

func (f Feed) IsUpdatedCorrectly() error {
	if f.Category == "" {
		return errors.New("missing feed category")
	}

	return nil
}

type FeedModel struct {
	DB PgxIface
	FeedModelInterface
}

func (m FeedModel) Add(ctx context.Context, f Feed) error {
	sqlCat := `INSERT INTO molo_category (name) VALUES ($1) ON CONFLICT DO NOTHING`

	sqlFeed := `INSERT INTO molo_feed (
	    name, category_id, user_id, author, url, rss_url, create_date, update_date 
	) values ($2, (SELECT id FROM molo_category WHERE name = $1), $3, $4, $5, $6, $7, $8)`

	tx, err := m.DB.Begin(context.Background())
	if err != nil {
		return errorConvert(err)
	}
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, sqlCat, f.Category)
	if err != nil {
		return errorConvert(err)
	}

	_, err = tx.Exec(ctx, sqlFeed, f.Category, f.Name, f.UserID, f.Author, f.URL, f.RssURL, f.CreateDate, f.UpdateDate)
	if err != nil {
		return errorConvert(err)
	}

	err = tx.Commit(ctx)
	return errorConvert(err)
}

func (m FeedModel) Get(ctx context.Context, userID, feedID string) (Feed, error) {
	sql := `SELECT f.id, f.name, f.category_id, c.name as category_name, f.user_id, 
		u.name as user_name, f.author, f.url, f.rss_url, f.create_date, f.update_date 
		FROM molo_feed AS f 
		JOIN molo_category AS c ON f.category_id = c.id 
		JOIN molo_user AS u ON f.user_id = u.id 
		WHERE f.user_id = $1 AND f.id = $2`

	row := m.DB.QueryRow(ctx, sql, userID, feedID)

	var f Feed
	err := row.Scan(&f.Id, &f.Name, &f.CategoryID, &f.Category, &f.UserID,
		&f.User, &f.Author, &f.URL, &f.RssURL, &f.CreateDate, &f.UpdateDate)
	return f, errorConvert(err)
}

func (m FeedModel) GetAll(ctx context.Context, userID string) ([]Feed, error) {
	// TODO pagination
	sql := `SELECT f.id, f.name, f.category_id, c.name as category_name, f.user_id, 
		u.name as user_name, f.author, f.url, f.rss_url, f.create_date, f.update_date 
		FROM molo_feed AS f 
		JOIN molo_category AS c ON f.category_id = c.id 
		JOIN molo_user AS u ON f.user_id = u.id 
		WHERE user_id = $1 ORDER BY create_date`

	rows, err := m.DB.Query(ctx, sql, userID)
	if err != nil {
		return nil, errorConvert(err)
	}
	defer rows.Close()

	var feeds []Feed
	for rows.Next() {
		var f Feed
		err = rows.Scan(&f.Id, &f.Name, &f.CategoryID, &f.Category, &f.UserID,
			&f.User, &f.Author, &f.URL, &f.RssURL, &f.CreateDate, &f.UpdateDate)
		if err != nil {
			return nil, errorConvert(err)
		}
		feeds = append(feeds, f)
	}
	if err = rows.Err(); err != nil {
		return nil, errorConvert(err)
	}
	return feeds, errorConvert(err)
}

func (m FeedModel) Delete(ctx context.Context, userID, feedID string) (int, error) {
	sqlEntries := `DELETE FROM molo_entry
		       WHERE user_id = $1 AND feed_id = $2`

	sqlFeed := `DELETE FROM molo_feed
		    WHERE user_id = $1 AND id = $2`

	tx, err := m.DB.Begin(context.Background())
	if err != nil {
		return 0, errorConvert(err)
	}
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, sqlEntries, userID, feedID)
	if err != nil {
		return 0, errorConvert(err)
	}

	tag, err := tx.Exec(ctx, sqlFeed, userID, feedID)
	if err != nil {
		return 0, errorConvert(err)
	}

	err = tx.Commit(ctx)
	return int(tag.RowsAffected()), errorConvert(err)
}

func (m FeedModel) Update(ctx context.Context, f Feed) error {
	sqlCat := `INSERT INTO molo_category (name) VALUES ($1) ON CONFLICT DO NOTHING`

	sqlFeed := `UPDATE molo_feed
		    SET category_id = (SELECT id FROM molo_category WHERE name = $1)
		    WHERE id = $2 AND user_id = $3`

	tx, err := m.DB.Begin(context.Background())
	if err != nil {
		return errorConvert(err)
	}
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, sqlCat, f.Category)
	if err != nil {
		return errorConvert(err)
	}

	_, err = tx.Exec(ctx, sqlFeed, f.Category, f.Id, f.UserID)
	if err != nil {
		return errorConvert(err)
	}

	err = tx.Commit(ctx)
	return errorConvert(err)
}
