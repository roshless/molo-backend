package model

import (
	"context"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
)

type UserModelInterface interface {
	Add(ctx context.Context, u User) error
	Get(ctx context.Context, name string) (User, error)
	GetAll(ctx context.Context) ([]User, error)
	Delete(ctx context.Context, name string) error
	Update(ctx context.Context, name string, u User) error
	Authenticate(ctx context.Context, u User) (User, error)
}

type CategoryModelInterface interface {
	Add(ctx context.Context, c Category) error
	Get(ctx context.Context, name string) (Category, error)
}

type FeedModelInterface interface {
	Add(ctx context.Context, f Feed) error
	Get(ctx context.Context, userID, feedID string) (Feed, error)
	GetAll(ctx context.Context, userID string) ([]Feed, error)
	Delete(ctx context.Context, userID, feedID string) (int, error)
	Update(ctx context.Context, f Feed) error
}

type FeedEntryModelInterface interface {
	Add(ctx context.Context, f []FeedEntry) error
	GetAllOfFeed(ctx context.Context, userID, feedID string) ([]FeedEntry, error)
	GetAllUnreadOfFeed(ctx context.Context, userID, feedID string) ([]FeedEntry, error)
	GetAllUnread(ctx context.Context, userID string) ([]FeedEntry, error)
	Get(ctx context.Context, userID, feedID, entryID string) (FeedEntry, error)
	SetRead(ctx context.Context, userID, feedID, entryID string) error
	Delete(ctx context.Context, userID, feedID string) (int, error)
}

type PgxIface interface {
	Begin(context.Context) (pgx.Tx, error)
	Exec(context.Context, string, ...interface{}) (pgconn.CommandTag, error)
	QueryRow(context.Context, string, ...interface{}) pgx.Row
	Query(context.Context, string, ...interface{}) (pgx.Rows, error)
	Ping(context.Context) error
	Close()
}
