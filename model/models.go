package model

type Models struct {
	Users       UserModel
	Feeds       FeedModel
	Categories  CategoryModel
	FeedEntries FeedEntryModel
}

func NewModels(db PgxIface) Models {
	return Models{
		Users:       UserModel{DB: db},
		Feeds:       FeedModel{DB: db},
		Categories:  CategoryModel{DB: db},
		FeedEntries: FeedEntryModel{DB: db},
	}
}
