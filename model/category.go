package model

import (
	"context"
)

type Category struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type CategoryModel struct {
	DB PgxIface
	CategoryModelInterface
}

func (m CategoryModel) AddCategory(ctx context.Context, c Category) error {
	sql := `INSERT INTO molo_category (
		   name 
	       ) values ($1)
	       ON CONFLICT (name) DO NOTHING`

	_, err := m.DB.Exec(ctx, sql, c.Name)
	return errorConvert(err)
}

func (m CategoryModel) GetCategory(ctx context.Context, name string) (Category, error) {
	sql := `SELECT id, name FROM molo_category
	WHERE name = $1`

	row := m.DB.QueryRow(ctx, sql, name)

	var category Category
	err := row.Scan(&category.ID, &category.Name)
	return category, errorConvert(err)
}
