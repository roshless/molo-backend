package test

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"git.roshless.me/molo/backend/config"
	"git.roshless.me/molo/backend/controller"
	"git.roshless.me/molo/backend/model"
	"github.com/pashagolub/pgxmock/v2"
)

var (
	control *controller.Controller
	mock    pgxmock.PgxPoolIface
)

// executeRequest, creates a new ResponseRecorder
// then executes the request by calling ServeHTTP in the router
// after which the handler writes the response to the response recorder
// which we can then inspect.
func executeRequest(req *http.Request, s *controller.Controller) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	s.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

var (
	existingUser = model.User{
		Id: 1, Name: "admin", Password: "admin",
		Email: "randommail@example.org", IsAdmin: true,
		UpdateInterval: time.Duration(300000000000),
	}
	newUser = model.User{
		Id: 2, Name: "newuser", Password: "newuser",
		Email: "another@example.org", IsAdmin: false,
		UpdateInterval: time.Duration(300000000000),
	}
	existingFeed = model.Feed{
		Id: 1, UserID: 1,
		CategoryID: 1, Name: "admin", Author: "admin",
		URL: "randommail@example.org", RssURL: "",
		CreateDate: time.Now(), UpdateDate: time.Now(),
	}
	existingCategory = model.Category{
		ID: 1, Name: "category1",
	}
)

// getToken tries to login, returns JWT and Rows with the same user in token.
func getToken(t *testing.T) (string, *pgxmock.Rows) {
	jsonUser, _ := json.Marshal(&existingUser)

	// 2 rows- each query execution CONSUMES one for some reason
	rows := mock.NewRows([]string{"id", "name", "email", "is_admin", "update_interval"}).
		AddRow(existingUser.Id, existingUser.Name, existingUser.Email, existingUser.IsAdmin, existingUser.UpdateInterval).
		AddRow(existingUser.Id, existingUser.Name, existingUser.Email, existingUser.IsAdmin, existingUser.UpdateInterval) //TODO move to expectquery

	mock.ExpectQuery("FROM molo_user").
		WithArgs(existingUser.Name, existingUser.Password).
		WillReturnRows(rows)

	req, _ := http.NewRequest(http.MethodPost, "/api/v1/user/login", bytes.NewReader(jsonUser))
	req.Header.Add("Content-Type", "application/json")
	response := executeRequest(req, control)
	checkResponseCode(t, http.StatusOK, response.Code)
	return response.Body.String(), rows
}

func TestMain(m *testing.M) {
	var cfg = config.Config{
		Port:                       "1234",
		Periodic_feeds_updates:     false,
		Registration_enabled:       true,
		Database_connection_string: "",
		JWTSecret:                  "donotchange",
	}

	var err error
	mock, err = pgxmock.NewPool()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer mock.Close()
	control = controller.NewController(&cfg, mock)
	control.MountHandlers()

	os.Exit(m.Run())
}
