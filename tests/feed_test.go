package test

import (
	"encoding/json"
	"net/http"
	"strconv"
	"testing"

	"github.com/stretchr/testify/require"
)

// TODO test all things AND remeber about partial feed data
func TestGetAllFeeds(t *testing.T) {
	token, _ := getToken(t)

	rows := mock.NewRows([]string{
		"f.id", "f.name", "f.category_id", "category_name", "f.user_id", "user_name",
		"f.author", "f.url", "f.rss_url", "f.create_date", "f.update_date"}).
		AddRow(existingFeed.Id, existingFeed.Name, existingFeed.CategoryID, existingCategory.Name,
			existingFeed.UserID, existingFeed.User, existingFeed.Author, existingFeed.URL,
			existingFeed.RssURL, existingFeed.CreateDate, existingFeed.UpdateDate)
	mock.ExpectQuery("FROM molo_feed").
		WithArgs(strconv.Itoa(existingUser.Id)).
		WillReturnRows(rows)

	req, _ := http.NewRequest(http.MethodGet, "/api/v1/feed", nil)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)
	response := executeRequest(req, control)

	checkResponseCode(t, http.StatusOK, response.Code)

	// Returned feeds have more info
	feedsTemp := existingFeed
	feedsTemp.Category = existingCategory.Name
	feeds, _ := json.Marshal(&feedsTemp)
	require.Equal(t, "["+string(feeds)+"]\n", response.Body.String())

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}
}
