package test

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRoot(t *testing.T) {
	req, _ := http.NewRequest(http.MethodGet, "/", nil)
	response := executeRequest(req, control)

	checkResponseCode(t, http.StatusNotFound, response.Code)
	require.Equal(t, "404 page not found\n", response.Body.String())
}

func TestHeartbeat(t *testing.T) {
	req, _ := http.NewRequest(http.MethodGet, "/up", nil)
	response := executeRequest(req, control)

	checkResponseCode(t, http.StatusOK, response.Code)
	require.Equal(t, ".", response.Body.String())
}

func TestMetrics(t *testing.T) {
	req, _ := http.NewRequest(http.MethodGet, "/metrics", nil)
	response := executeRequest(req, control)

	checkResponseCode(t, http.StatusOK, response.Code)
}
