package test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/url"
	"testing"
	"time"

	"git.roshless.me/molo/backend/model"
	"github.com/pashagolub/pgxmock/v2"
	"github.com/stretchr/testify/require"
)

func TestLogin(t *testing.T) {
	// TODO should also test partial login aka only username and password (normal usecase)
	// Currently only tests all fields
	getToken(t)

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}
}

func TestGetAllUsers(t *testing.T) {
	token, rows := getToken(t)

	mock.ExpectQuery("FROM molo_user").
		WillReturnRows(rows)

	req, _ := http.NewRequest(http.MethodGet, "/api/v1/user", nil)
	req.Header.Add("Authorization", "Bearer "+token)
	response := executeRequest(req, control)

	// User list doesn't expose passwords
	tempUser := existingUser
	tempUser.Password = ""
	jsonUser, _ := json.Marshal(&tempUser)

	checkResponseCode(t, http.StatusOK, response.Code)
	require.Equal(t, "["+string(jsonUser)+"]\n", response.Body.String())

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}
}

func TestGetUser(t *testing.T) {
	token, rows := getToken(t)

	mock.ExpectQuery("FROM molo_user").
		WithArgs(existingUser.Id).
		WillReturnRows(rows)

	req, _ := http.NewRequest(http.MethodGet, "/api/v1/user/1", nil)
	req.Header.Add("Authorization", "Bearer "+token)
	response := executeRequest(req, control)

	// User list doesn't expose passwords
	tempUser := existingUser
	tempUser.Password = ""
	jsonUser, _ := json.Marshal(&tempUser)

	checkResponseCode(t, http.StatusOK, response.Code)
	require.Equal(t, string(jsonUser)+"\n", response.Body.String())

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}
}

func TestAddUser(t *testing.T) {
	mock.ExpectExec("INSERT INTO molo_user").
		WithArgs(newUser.Name, newUser.Password, newUser.Email, newUser.IsAdmin, newUser.UpdateInterval).
		WillReturnResult(pgxmock.NewResult("INSERT", 1))

	jsonUser, _ := json.Marshal(&newUser)
	req, _ := http.NewRequest(http.MethodPost, "/api/v1/user", bytes.NewReader(jsonUser))
	req.Header.Add("Content-Type", "application/json")
	response := executeRequest(req, control)

	checkResponseCode(t, http.StatusCreated, response.Code)
	require.Equal(t, "", response.Body.String())

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}
}

func TestDeleteUser(t *testing.T) {
	token, _ := getToken(t)

	mock.ExpectExec("DELETE FROM molo_user").
		WithArgs(existingUser.Id).
		WillReturnResult(pgxmock.NewResult("DELETE", 1))

	req, _ := http.NewRequest(http.MethodDelete, "/api/v1/user/1", nil)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)
	response := executeRequest(req, control)

	checkResponseCode(t, http.StatusOK, response.Code)
	require.Equal(t, "", response.Body.String())

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}
}

func TestSelfUser(t *testing.T) {
	token, rows := getToken(t)

	mock.ExpectQuery("FROM molo_user").
		WithArgs(existingUser.Id).
		WillReturnRows(rows)

	req, _ := http.NewRequest(http.MethodGet, "/api/v1/user/self", nil)
	req.Header.Add("Authorization", "Bearer "+token)
	response := executeRequest(req, control)

	// User list doesn't expose passwords
	tempUser := existingUser
	tempUser.Password = ""
	jsonUser, _ := json.Marshal(&tempUser)

	checkResponseCode(t, http.StatusOK, response.Code)
	require.Equal(t, string(jsonUser)+"\n", response.Body.String())

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}
}

func TestUpdateSelfUser(t *testing.T) {
	token, rows := getToken(t)

	updatedUser := model.User{
		Password:       "Airohb1OhH8dah3Oquamei9phohs0ohshaigh",
		Email:          "test12345@example.org",
		UpdateInterval: time.Duration(600000000000),
	}

	mock.ExpectQuery("FROM molo_user").
		WithArgs(existingUser.Id).
		WillReturnRows(rows)

	mock.ExpectExec("UPDATE molo_user SET").
		WithArgs(updatedUser.Password, updatedUser.Email, updatedUser.UpdateInterval, existingUser.Id).
		WillReturnResult(pgxmock.NewResult("UPDATE", 1))

	jsonUser, _ := json.Marshal(&updatedUser)
	req, _ := http.NewRequest(http.MethodPatch, "/api/v1/user", bytes.NewReader(jsonUser))
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)
	response := executeRequest(req, control)

	checkResponseCode(t, http.StatusOK, response.Code)
	require.Equal(t, "", response.Body.String())

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}
}

func TestUpdateUser(t *testing.T) {
	token, _ := getToken(t)

	mock.ExpectExec("UPDATE molo_user SET").
		WithArgs(newUser.Name, newUser.Password, newUser.Email, newUser.IsAdmin, newUser.UpdateInterval, existingUser.Id).
		WillReturnResult(pgxmock.NewResult("UPDATE", 1))

	jsonUser, _ := json.Marshal(&newUser)
	req, _ := http.NewRequest(http.MethodPut, "/api/v1/user/1", bytes.NewReader(jsonUser))
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)
	response := executeRequest(req, control)

	checkResponseCode(t, http.StatusOK, response.Code)
	require.Equal(t, "", response.Body.String())

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}
}

func TestSearchUser(t *testing.T) {
	token, rows := getToken(t)

	mock.ExpectQuery("FROM molo_user").
		WithArgs(existingUser.Name).
		WillReturnRows(rows)

	req, _ := http.NewRequest(http.MethodGet, "/api/v1/user/search", nil)
	q := url.Values{}
	q.Add("username", "admin")
	req.URL.RawQuery = q.Encode()
	req.Header.Add("Authorization", "Bearer "+token)
	response := executeRequest(req, control)

	// User list doesn't expose passwords
	tempUser := existingUser
	tempUser.Password = ""
	jsonUser, _ := json.Marshal(&tempUser)

	checkResponseCode(t, http.StatusOK, response.Code)
	require.Equal(t, string(jsonUser)+"\n", response.Body.String())

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatal(err)
	}
}
